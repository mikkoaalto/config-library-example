package fi.trinil.config.example;

import fi.trinil.config.DefaultConfiguration;
import fi.trinil.config.util.ConfigurationKey;

public interface SoftwareConfiguration extends DefaultConfiguration {

	public String getFiTrinilClientName();
	
	public String getFiTrinilClientValue();
	
	public String[] getFiTrinilClientValues();
	
	@ConfigurationKey("fi.trinil.client.name")
	public String getOneParameter();
	
}
