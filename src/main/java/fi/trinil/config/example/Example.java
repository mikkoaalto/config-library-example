package fi.trinil.config.example;

import fi.trinil.config.ConfigurationFactory;
import fi.trinil.config.util.ConfigurationRepository;

public class Example {

	private final ConfigurationFactory factory = new ConfigurationFactory();

	private SoftwareConfiguration conf;


	public void run() {
		System.out.println("FiTrinilClientName {fi.trinil.client.name}" + conf.getFiTrinilClientName());

		String[] fiTrinilClientValues = conf.getFiTrinilClientValues();
		System.out.println("FiTrinilClientValues {fi.trinil.client.values[n]}");
		for (String s : fiTrinilClientValues)
			System.out.println("\t" + s);

		System.out.println("FiTrinilClientValue {fi.trinil.client.value} " + conf.getFiTrinilClientValue());
		
		System.out.println("With the annotation " + conf.getOneParameter());
	}

	public static void main( String[] args ) {

		System.setProperty(ConfigurationRepository.CONFIG_PROCESS, "config_client");
		System.setProperty(ConfigurationRepository.CONFIG_ENVIRONMENT, "dev2");
		System.setProperty(ConfigurationRepository.CONFIG_SERVER, "localhost:8888");
		
		System.setProperty(ConfigurationFactory.CONFIG_RELOAD_DELAY, "10000");
		
		Example example = new Example();
		example.conf = (SoftwareConfiguration)example.factory.createConfiguration(SoftwareConfiguration.class);
		example.factory.setupAutoRefresh();
		example.run();
		
		example.factory.stopAutoRefresh();
	}
}
