package fi.elisa.config_library.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.jmock.Expectations;
import org.junit.runner.RunWith;

import fi.elisa.config_library.ConfigurationFactory;
import fi.elisa.config_library.DefaultConfiguration;
import fi.elisa.config_library.domain.Configuration;
import fi.elisa.config_library.domain.PropertySource;
import fi.elisa.config_library.util.ConfigurationProxyTest.OwnTestConfiguration;
import jdave.Specification;
import jdave.junit4.JDaveRunner;

@RunWith(JDaveRunner.class)
public class ConfigurationProxyTest extends Specification<OwnTestConfiguration> {


	private static final String STRING_VALUE = "valueStr";
	private static final String STR_VAL_KEY = "own.string.val";
	
	private static final Integer INT_VALUE = 1;
	private static final String INT_VAL_KEY = "own.int.val";
	
	private static final String[] STR_ARR_VALUE = {"1", "2"};
	private static final String STR_ARR_VAL_KEY = "own.string.arr.val";
	
	
	public abstract class BaseConfiguration {

		ConfigurationRepository confRepo = mock(ConfigurationRepository.class);
		Configuration config = new Configuration();
		
		public OwnTestConfiguration create() {

			initConfig();
			
			ConfigurationFactory factory = new ConfigurationFactory();
			factory.getProxy().setConfRepo(confRepo);
			return (OwnTestConfiguration)factory.createConfiguration(OwnTestConfiguration.class);
		}
		
		protected void addConfigExpectation() {
			checking(new Expectations() {{
				one(confRepo).getConfig();
				will(returnValue(config));
			}});
		}
		
		private void initConfig() {
			config.setPropertySources(new ArrayList<>());
			PropertySource e = new PropertySource();
			Map<String, String> map = new HashMap<>();
			map.put(STR_VAL_KEY, STRING_VALUE);
			map.put(STR_ARR_VAL_KEY+"[0]", STR_ARR_VALUE[0]);
			map.put(STR_ARR_VAL_KEY+"[1]", STR_ARR_VALUE[1]);
			map.put(INT_VAL_KEY, INT_VALUE.toString());
			e.setSource(map);
			config.getPropertySources().add(e);
		}
	}
	
	
	public class WhenKeysAreFound extends BaseConfiguration {
		

		public void stringIsReturned() {
			addConfigExpectation();
			String value = context.getOwnStringVal();
			specify(value, STRING_VALUE);
		}
		
		public void stringArrIsReturned() {
			checking(new Expectations() {{
				one(confRepo).getConfig();
				will(returnValue(config));
				one(confRepo).getConfig();
				will(returnValue(config));
			}});
			String[] value = context.getOwnStringArrVal();
			specify(value, containAll(STR_ARR_VALUE[0], STR_ARR_VALUE[1]));
		}
		
		public void integerIsReturned() {
			addConfigExpectation();
			Integer value = context.getOwnIntVal();
			specify(value, INT_VALUE);
		}
		
	}



	public interface OwnTestConfiguration extends DefaultConfiguration {

		public Integer getOwnIntVal();
		public String getOwnStringVal();
		public String[] getOwnStringArrVal();
	}

}
