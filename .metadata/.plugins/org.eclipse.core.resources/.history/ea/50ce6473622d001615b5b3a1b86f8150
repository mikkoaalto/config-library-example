package fi.elisa.config_library;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import fi.elisa.config_library.domain.Configuration;

public class ConfigurationProxy implements InvocationHandler {

	Logger log = LoggerFactory.getLogger(ConfigurationProxy.class.getName());
	
	private Configuration config;

	public void init() {
		RestTemplate restTemplate = new RestTemplate();
		config = restTemplate.getForObject("http://localhost:8888/config_client/dev2", Configuration.class);
	}

	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		if (isDefaultImplementation(method)) {
			return method.invoke(proxy, args);
		} else {
			return dynamicInvoke(method);
		}
	}

	private Object dynamicInvoke(Method method) {
		String key = parseKey(method.getName());
		Method getter = findMethod(method.getReturnType());
		try {
			return getter.invoke(this, key);
		} catch (IllegalAccessException|IllegalArgumentException|InvocationTargetException e) {
			log.error("Unable to call method", e);
		}
		throw new IllegalArgumentException("Could not find getter for " + method.getName());
	}

	private Method findMethod(Class<?> returnType) {
		String canonicalName = "get" + returnType.getCanonicalName();
		canonicalName.replaceAll("[]", "Array");
		log.debug("dyn parsed '" + canonicalName + "'");
		
//		if (method.getReturnType() == String.class)
//			return getString(key);
//		else if (method.getReturnType() == Integer.class)
//			return getInteger(key);
//		else if (method.getReturnType() == String[].class)
//			return getStringArray(key);
		
		throw new IllegalArgumentException("Return type " + method.getReturnType().getCanonicalName());
	}

	private String parseKey(String name) {
		String rawKey = split(name);
		rawKey = rawKey.toLowerCase();
		return rawKey.substring(rawKey.indexOf('.') + 1, rawKey.length());
	}

	private String split(String name) {
		return name.replaceAll(String.format("%s|%s|%s", "(?<=[A-Z])(?=[A-Z][a-z])", "(?<=[^A-Z])(?=[A-Z])", "(?<=[A-Za-z])(?=[^A-Za-z])"), ".");
	}

	private boolean isDefaultImplementation(Method method) {
		for (Method ownMethod : getClass().getMethods()) {
			if (ownMethod.equals(method))
				return true;
		}

		return false;
	}

	public String getString(String key) {
		return (String)getActualValue(key);
	}

	public Integer getInteger(String key) {
		return Integer.parseInt(getString(key));
	}

	public String[] getStringArray(String key) {
		int index = 0;
		List<String> values = new ArrayList<>();
		while (getValueWithIndex(key, index) != null) {
			values.add(getValueWithIndex(key, index++));
		}
		return values.toArray(new String[0]);
	}

	private String getValueWithIndex(String key, int index) {
		return (String)getActualValue(createIndex(key, index));
	}

	private String createIndex(String key, int index) {
		return String.format("%s[%d]", key, index);
	}

	private Object getActualValue(String key) {
		log.debug("Trying to find configuration: " + key);
		return config.getPropertySources().get(0).getSource().get(key);
	}

}
