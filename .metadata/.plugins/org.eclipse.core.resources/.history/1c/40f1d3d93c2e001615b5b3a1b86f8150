package fi.elisa.config_library;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import fi.elisa.config_library.domain.Configuration;

@Service
public class ConfigurationRepository {

	private static final String HTTP = "http://";
	private static final String DEFAULT_RETRY_COUNT = "0";
	private static final String DEFAULT_RETRY_DELAY = "5000";
	private static final String DEFAULT_CONFIG_SERVER = "localhost:8888";
	private static final String CONFIG_SERVER = "CONFIG_SERVER";
	private static final String CONFIG_ENVIRONMENT = "CONFIG_ENVIRONMENT";
	private static final String CONFIG_PROCESS = "CONFIG_PROCESS";
	private static final String CONFIG_RETRY_COUNT = "CONFIG_RETRY_COUNT";
	private static final String CONFIG_RETRY_DELAY = "CONFIG_RETRY_DELAY";
	
	private Configuration config;
	private final String configProcess;
	private final String configEnv;
	private final String[] servers;
	private final int maxRetryCount;
	private final long retryDelay;
	
	public ConfigurationRepository() {
		configProcess = System.getProperty(CONFIG_PROCESS, "config_client");
		configEnv = System.getProperty(CONFIG_ENVIRONMENT, "dev2");
		maxRetryCount = Integer.parseInt(System.getProperty(CONFIG_RETRY_COUNT, DEFAULT_RETRY_COUNT));
		retryDelay = Long.parseLong(System.getProperty(CONFIG_RETRY_DELAY, DEFAULT_RETRY_DELAY));
		servers = getServers();
	}

	private String[] getServers() {
		String servers = System.getProperty(CONFIG_SERVER, DEFAULT_CONFIG_SERVER);
		return servers.split(";");
	}
	
	public synchronized Configuration getConfig() {
		if (config == null)
			readConfig();
		
		return config;
	}

	private void readConfig() {
		
		int count = 0;
		
		while (config == null && count <= maxRetryCount) {
			readFromServers(servers);
			
			try {
				Thread.sleep(retryDelay);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			count ++;
		}
	}

	private void readFromServers(String[] servers) {
		for (String server : servers) {
			RestTemplate restTemplate = new RestTemplate();
			config  = restTemplate.getForObject(createURL(server), Configuration.class);
			if (config != null)
				return;
		}

	}

	private String createURL(String server) {
		return HTTP + server + "/" + configProcess + "/" + configEnv;
	}
}
