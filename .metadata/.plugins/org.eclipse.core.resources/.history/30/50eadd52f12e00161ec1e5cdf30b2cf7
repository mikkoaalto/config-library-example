package fi.elisa.config_library.util;

import org.junit.runner.RunWith;

import fi.elisa.config_library.ConfigurationFactory;
import fi.elisa.config_library.DefaultConfiguration;
import fi.elisa.config_library.util.ConfigurationProxyTest.OwnTestConfiguration;
import jdave.Specification;
import jdave.junit4.JDaveRunner;

@RunWith(JDaveRunner.class)
public class ConfigurationProxyTest extends Specification<OwnTestConfiguration> {


	private static final String STRING_VALUE = "valueStr";
	private static final Integer INT_VALUE = 1;
	private static final String[] STR_ARR_VALUE = {"1", "2"};
	
	
	public abstract class BaseConfiguration {

		ConfigurationRepository confRepo = mock(ConfigurationRepository.class);
		
		public OwnTestConfiguration create() {

			ConfigurationFactory factory = new ConfigurationFactory();
			factory.getProxy().setConfRepo(confRepo);
			return (OwnTestConfiguration)factory.createConfiguration(OwnTestConfiguration.class);
		}
	}
	
	
	public class WhenKeysAreFound extends BaseConfiguration {
		
		public void stringIsReturned() {
			String value = context.getString("foo");
			specify(value, STRING_VALUE);
		}
		
		public void stringArrIsReturned() {
			String value = context.getString("foo");
			specify(value, STR_ARR_VALUE);
		}
		
		public void integerIsReturned() {
			String value = context.getString("foo");
			specify(value, INT_VALUE);
		}
		
	}



	public interface OwnTestConfiguration extends DefaultConfiguration {

		public Integer ownInt();
		public String ownString();
		public String[] ownStringArr();
	}

}
