package fi.elisa.config_library.util;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.stereotype.Component;

@Component
public class ConfigurationProxy implements InvocationHandler {

	private final Logger log = Logger.getLogger(ConfigurationProxy.class.getName());
	
	private ConfigurationRepository confRepo = new ConfigurationRepository();
	
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		Method defaultMethod = isDefaultImplementation(method);
		if (defaultMethod != null) {
			return defaultMethod.invoke(proxy, args);
		} else {
			return dynamicInvoke(method);
		}
	}

	private Object dynamicInvoke(Method method) {
		try {
			String key = parseKey(method.getName());
			Method getter = findMethod(method.getReturnType());
			
			return getter.invoke(this, key);
		} catch (IllegalAccessException|IllegalArgumentException|InvocationTargetException e) {
			log.severe("Unable to call method " + e);
			e.printStackTrace();
		}
		throw new IllegalArgumentException("Could not find getter for " + method.getName());
	}

	private Method findMethod(Class<?> returnType) {
		String canonicalName = "get" + returnType.getSimpleName();
		canonicalName = canonicalName.replaceAll("\\[", "Array").replaceAll("\\]", "");
		log.finest("dyn parsed '" + canonicalName + "'");
		for (Method method : this.getClass().getMethods())
			if (method.getName().equals(canonicalName) && method.getReturnType().equals(returnType))
				return method;
		
		throw new IllegalArgumentException("Return type " + returnType.getCanonicalName());
	}

	private String parseKey(String name) {
		String rawKey = split(name);
		rawKey = rawKey.toLowerCase();
		return rawKey.substring(rawKey.indexOf('.') + 1, rawKey.length());
	}

	private String split(String name) {
		return name.replaceAll(String.format("%s|%s|%s", 
				"(?<=[A-Z])(?=[A-Z][a-z])", 
				"(?<=[^A-Z])(?=[A-Z])", 
				"(?<=[A-Za-z])(?=[^A-Za-z])"), 
				".");
	}

	private Method isDefaultImplementation(Method method) {
		
		for (Method ownMethod : getClass().getMethods()) {
			if (ownMethod.getName().equals(method.getName()) &&
					ownMethod.getReturnType().equals(method.getReturnType()) && ownMethod.isSynthetic())
				return ownMethod;
		}
		
		return null;
	}

	public String getString(String key) {
		return (String)getActualValue(key);
	}

	public Integer getInteger(String key) {
		return Integer.parseInt(getString(key));
	}

	public String[] getStringArray(String key) {
		int index = 0;
		List<String> values = new ArrayList<>();
		while (getValueWithIndex(key, index) != null) {
			values.add(getValueWithIndex(key, index++));
		}
		return values.toArray(new String[0]);
	}

	private String getValueWithIndex(String key, int index) {
		return (String)getActualValue(createIndex(key, index));
	}

	private String createIndex(String key, int index) {
		return String.format("%s[%d]", key, index);
	}

	private Object getActualValue(String key) {
		log.finest("Trying to find configuration: " + key);
		return getConfRepo().getConfig().getPropertySources().get(0).getSource().get(key);
	}

	public ConfigurationRepository getConfRepo() {
		return confRepo;
	}

	public void setConfRepo(ConfigurationRepository confRepo) {
		this.confRepo = confRepo;
	}

}
