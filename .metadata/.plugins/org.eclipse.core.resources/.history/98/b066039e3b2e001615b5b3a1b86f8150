package fi.elisa.config_library;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import fi.elisa.config_library.domain.Configuration;

@Service
public class ConfigurationRepository {

	private static final String DEFAULT_CONFIG_SERVER = "localhost:8888";
	private static final String CONFIG_SERVER = "CONFIG_SERVER";
	private static final String CONFIG_ENVIRONMENT = "CONFIG_ENVIRONMENT";
	private static final String CONFIG_PROCESS = "CONFIG_PROCESS";
	private static final String CONFIG_RETRY_COUNT = "CONFIG_RETRY_COUNT";
	private static final String CONFIG_RETRY_DELAY = "CONFIG_RETRY_DELAY";
	
	private static final long RETRY_DELAY = 5000;
	
	private Configuration config;
	private final String configProcess;
	private final String configEnv;
	private final String[] servers;
	
	private final int maxRetryCount;
	
	public ConfigurationRepository() {
		configProcess = System.getProperty(CONFIG_PROCESS, "config_client");
		configEnv = System.getProperty(CONFIG_ENVIRONMENT, "dev2");
		maxRetryCount = Integer.parseInt(System.getProperty(CONFIG_RETRY_COUNT, "0"));
		servers = getServers();
	}

	private String[] getServers() {
		String servers = System.getProperty(CONFIG_SERVER, DEFAULT_CONFIG_SERVER);
		return servers.split(";");
	}
	
	public synchronized Configuration getConfig() {
		if (config == null)
			readConfig();
		
		return config;
	}

	private void readConfig() {
		
		int count = 0;
		
		while (config == null && count <= maxRetryCount) {
			readFromServers(servers);
			
			try {
				Thread.sleep(RETRY_DELAY);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			count ++;
		}
	}

	private void readFromServers(String[] servers) {
		for (String server : servers) {
			RestTemplate restTemplate = new RestTemplate();
			config  = restTemplate.getForObject("http://" + server + "/" + configProcess + "/" + configEnv, Configuration.class);
			if (config != null)
				return;
		}

	}
}
