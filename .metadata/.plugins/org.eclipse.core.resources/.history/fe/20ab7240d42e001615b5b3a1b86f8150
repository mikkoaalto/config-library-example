package fi.elisa.config_library.util;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import fi.elisa.config_library.domain.Configuration;

@Service
public class ConfigurationRepository {

	private static final String PROTOCOL = "http://";
	private static final String DEFAULT_RETRY_COUNT = "1";
	private static final String DEFAULT_RETRY_DELAY = "5000";
	private static final String DEFAULT_CONFIG_SERVER = "localhost:8888";
	
	public static final String CONFIG_SERVER = "CONFIG_SERVER";
	public static final String CONFIG_ENVIRONMENT = "CONFIG_ENVIRONMENT";
	public static final String CONFIG_PROCESS = "CONFIG_PROCESS";
	public static final String CONFIG_RETRY_COUNT = "CONFIG_RETRY_COUNT";
	public static final String CONFIG_RETRY_DELAY = "CONFIG_RETRY_DELAY";
	
	private Configuration config;
	private final String configProcess;
	private final String configEnv;
	private final String[] servers;
	private final int maxRetryCount;
	private final long retryDelay;
	private RestTemplate restTemplate = new RestTemplate();
	
	public ConfigurationRepository() {
		configProcess = System.getProperty(CONFIG_PROCESS, "");
		configEnv = System.getProperty(CONFIG_ENVIRONMENT, "");
		maxRetryCount = Integer.parseInt(System.getProperty(CONFIG_RETRY_COUNT, DEFAULT_RETRY_COUNT));
		retryDelay = Long.parseLong(System.getProperty(CONFIG_RETRY_DELAY, DEFAULT_RETRY_DELAY));
		servers = getServers();
	}

	private String[] getServers() {
		String servers = System.getProperty(CONFIG_SERVER, DEFAULT_CONFIG_SERVER);
		return servers.split(";");
	}
	
	public synchronized Configuration getConfig() {
		if (config == null)
			readConfig();
		
		return config;
	}

	private void readConfig() {
		
		int count = 0;
		
		while (retry(count)) {
			readFromServers(servers);
			
			if (config != null)
				return;
			
			waitToRetry();
			count ++;
		}
		
		if (config == null)
			throw new ConfigurationException("Cannot resolve configuration");
	}

	private void waitToRetry() {
		try {
			Thread.sleep(retryDelay);
		} catch (InterruptedException e) {
			throw new ConfigurationException("Cannot retry", e);
		}
	}

	private boolean retry(int count) {
		return count <= maxRetryCount;
	}

	private void readFromServers(String[] servers) {
		for (String server : servers) {
			try {
				read(createURL(server));
				return;
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

	}

	private void read(String server) {
		config = restTemplate.getForObject(server, Configuration.class);
	}

	private String createURL(String server) {
		return PROTOCOL + server + "/" + configProcess + "/" + configEnv;
	}
	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
}
