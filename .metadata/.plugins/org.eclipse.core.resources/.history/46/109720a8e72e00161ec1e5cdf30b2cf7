package fi.elisa.config_library.util;

import static org.junit.Assert.assertEquals;

import org.jmock.Expectations;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.web.client.RestTemplate;

import fi.elisa.config_library.domain.Configuration;
import jdave.Specification;
import jdave.junit4.JDaveRunner;

@RunWith(JDaveRunner.class)
public class ConfigurationRepositoryTest extends Specification<ConfigurationRepository> {


	public class WhenConfigurationReadedSuccessfully {
		
		private RestTemplate restTemplateMock;

		public ConfigurationRepository create() {
			System.setProperty(ConfigurationRepository.CONFIG_PROCESS, "proc");
			System.setProperty(ConfigurationRepository.CONFIG_ENVIRONMENT, "env");

			ConfigurationRepository repo = new ConfigurationRepository();
			restTemplateMock = mock(RestTemplate.class);
			repo.setRestTemplate(restTemplateMock);
			return repo;
		}

		public void theConfigurationIsReturned() {
			final Configuration value = new Configuration();

			checking(new Expectations() {{
				one(restTemplateMock).getForObject("http://localhost:8888/proc/env", Configuration.class);
				returnValue(value);
			}});


			Configuration config = repo.getConfig();

			assertEquals(config, value);			
		}
		
	}
	
	@Test
	public void readConfigurationTwice() {
		System.setProperty(ConfigurationRepository.CONFIG_PROCESS, "proc");
		System.setProperty(ConfigurationRepository.CONFIG_ENVIRONMENT, "env");

		ConfigurationRepository repo = new ConfigurationRepository();
		RestTemplate restTemplateMock = mock(RestTemplate.class);
		repo.setRestTemplate(restTemplateMock);

		final Configuration value = new Configuration();
		checking(new Expectations() {{
			one(restTemplateMock.getForObject("http://localhost:8888/proc/env", Configuration.class));
			returnValue(value);
		}});

		Configuration config = repo.getConfig();
		Configuration config2 = repo.getConfig();

		assertEquals(config, value);
		assertEquals(config, config2);
	}

	@Test
	public void readConfigurationFromSecondServerSuccessfully() {
		System.setProperty(ConfigurationRepository.CONFIG_SERVER, "localhost:8889;localhost:8888");
		System.setProperty(ConfigurationRepository.CONFIG_PROCESS, "proc");
		System.setProperty(ConfigurationRepository.CONFIG_ENVIRONMENT, "env");

		ConfigurationRepository repo = new ConfigurationRepository();
		RestTemplate restTemplateMock = mock(RestTemplate.class);
		repo.setRestTemplate(restTemplateMock);

		final Configuration value = new Configuration();
		checking(new Expectations() {{
			one(restTemplateMock.getForObject("http://localhost:8889/proc/env", Configuration.class));
			throwException(new RuntimeException());

			one(restTemplateMock.getForObject("http://localhost:8888/proc/env", Configuration.class));
			returnValue(null);
		}});

		Configuration config = repo.getConfig();

		assertEquals(config, value);
	}

	@Test(expected = ConfigurationException.class)
	public void readConfigurationFails() {
		System.setProperty(ConfigurationRepository.CONFIG_SERVER, "localhost:8889;localhost:8888");
		System.setProperty(ConfigurationRepository.CONFIG_PROCESS, "proc");
		System.setProperty(ConfigurationRepository.CONFIG_ENVIRONMENT, "env");

		ConfigurationRepository repo = new ConfigurationRepository();
		RestTemplate restTemplateMock = mock(RestTemplate.class);
		repo.setRestTemplate(restTemplateMock);

		checking(new Expectations() {{
			one(restTemplateMock.getForObject("http://localhost:8889/proc/env", Configuration.class));
			throwException(new RuntimeException());
			one(restTemplateMock.getForObject("http://localhost:8888/proc/env", Configuration.class));
			returnValue(null);
		}});

		repo.getConfig();
	}

}
