package fi.elisa.config.util;

import java.util.logging.Logger;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import fi.elisa.config.domain.Configuration;

@Service
public class ConfigurationRepository {

	private static final Logger log = Logger.getLogger(ConfigurationRepository.class.getName());
	
	private static final String PROTOCOL = "http://";
	private static final String DEFAULT_RETRY_COUNT = "0";
	private static final String DEFAULT_RETRY_DELAY = "5000";
	private static final String DEFAULT_CONFIG_SERVER = "localhost:8888";
	
	public static final String CONFIG_SERVER = "CONFIG_SERVER";
	public static final String CONFIG_ENVIRONMENT = "CONFIG_ENVIRONMENT";
	public static final String CONFIG_PROCESS = "CONFIG_PROCESS";
	public static final String CONFIG_RETRY_COUNT = "CONFIG_RETRY_COUNT";
	public static final String CONFIG_RETRY_DELAY = "CONFIG_RETRY_DELAY";
	
	private Configuration config;
	private final String configProcess;
	private final String configEnv;
	private final String[] servers;
	private final int maxRetryCount;
	private final long retryDelay;
	private RestTemplate restTemplate = new RestTemplate();
	
	public ConfigurationRepository() {
		configProcess = System.getProperty(CONFIG_PROCESS, "");
		configEnv = System.getProperty(CONFIG_ENVIRONMENT, "");
		maxRetryCount = Integer.parseInt(System.getProperty(CONFIG_RETRY_COUNT, DEFAULT_RETRY_COUNT));
		retryDelay = Long.parseLong(System.getProperty(CONFIG_RETRY_DELAY, DEFAULT_RETRY_DELAY));
		servers = getServers();
	}

	private String[] getServers() {
		String servers = System.getProperty(CONFIG_SERVER, DEFAULT_CONFIG_SERVER);
		return servers.split(";");
	}
	
	public synchronized Configuration getConfig() {
		if (config == null)
			readConfig();
		
		return config;
	}

	private void readConfig() {
		
		int count = 0;
		
		while (true) {
			readFromServers(servers);
			
			if (config != null)
				return;
			
			if (!retry(++count))
				break;
			
			waitToRetry();
		}
		
		throw new ConfigurationException("Cannot resolve configuration");
	}

	private void waitToRetry() {
		try {
			Thread.sleep(retryDelay);
		} catch (InterruptedException e) {
			throw new ConfigurationException("Cannot retry", e);
		}
	}

	private boolean retry(int count) {
		return count <= maxRetryCount;
	}

	private void readFromServers(String[] servers) {
		for (String server : servers) {
			String url = createURL(server);
			try {
				log.finest("Reading configuration from server " + url);
				read(url);
				if (config != null)
					return;
			} catch (Exception ex) {
				log.warning("Cannot resolve configuration from server " + url);
			}
		}

	}

	private void read(String server) {
		System.out.println("rest " + restTemplate.toString());
		config = restTemplate.getForObject(server, Configuration.class);
		System.out.println("CONFIG " + config);
	}

	private String createURL(String server) {
		return PROTOCOL + server + "/" + configProcess + "/" + configEnv;
	}
	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
}
