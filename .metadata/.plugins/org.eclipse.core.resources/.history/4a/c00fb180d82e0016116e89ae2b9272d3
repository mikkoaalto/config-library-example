package fi.elisa.config_library.util;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.web.client.RestTemplate;

import fi.elisa.config_library.domain.Configuration;
import jdave.junit4.JDaveRunner;

@RunWith(JDaveRunner.class)
public class ConfigurationRepositoryTest {


	@Test
	public void readConfigurationSuccessfully() {
		System.setProperty(ConfigurationRepository.CONFIG_PROCESS, "proc");
		System.setProperty(ConfigurationRepository.CONFIG_ENVIRONMENT, "env");
		
		ConfigurationRepository repo = new ConfigurationRepository();
		RestTemplate restTemplateMock = mock(RestTemplate.class);
		repo.setRestTemplate(restTemplateMock);
		
		Configuration value = new Configuration();
		when(restTemplateMock.getForObject("http://localhost:8888/proc/env", Configuration.class)).thenReturn(value);
		
		Configuration config = repo.getConfig();
		
		assertEquals(config, value);
	}
	
	@Test
	public void readConfigurationTwice() {
		System.setProperty(ConfigurationRepository.CONFIG_PROCESS, "proc");
		System.setProperty(ConfigurationRepository.CONFIG_ENVIRONMENT, "env");
		
		ConfigurationRepository repo = new ConfigurationRepository();
		RestTemplate restTemplateMock = mock(RestTemplate.class);
		repo.setRestTemplate(restTemplateMock);
		
		Configuration value = new Configuration();
		when(restTemplateMock.getForObject("http://localhost:8888/proc/env", Configuration.class)).thenReturn(value);
		
		Configuration config = repo.getConfig();
		Configuration config2 = repo.getConfig();
		
		assertEquals(config, value);
		assertEquals(config, config2);
	}
	
	@Test
	public void readConfigurationFromSecondServerSuccessfully() {
		System.setProperty(ConfigurationRepository.CONFIG_SERVER, "localhost:8889;localhost:8888");
		System.setProperty(ConfigurationRepository.CONFIG_PROCESS, "proc");
		System.setProperty(ConfigurationRepository.CONFIG_ENVIRONMENT, "env");
		
		ConfigurationRepository repo = new ConfigurationRepository();
		RestTemplate restTemplateMock = mock(RestTemplate.class);
		repo.setRestTemplate(restTemplateMock);
		
		Configuration value = new Configuration();
		when(restTemplateMock.getForObject("http://localhost:8889/proc/env", Configuration.class)).thenThrow(new RuntimeException());
		when(restTemplateMock.getForObject("http://localhost:8888/proc/env", Configuration.class)).thenReturn(null);
		
		Configuration config = repo.getConfig();
		
		assertEquals(config, value);
	}
	
	@Test(expected = ConfigurationException.class)
	public void readConfigurationFails() {
		System.setProperty(ConfigurationRepository.CONFIG_SERVER, "localhost:8889;localhost:8888");
		System.setProperty(ConfigurationRepository.CONFIG_PROCESS, "proc");
		System.setProperty(ConfigurationRepository.CONFIG_ENVIRONMENT, "env");
		
		ConfigurationRepository repo = new ConfigurationRepository();
		RestTemplate restTemplateMock = mock(RestTemplate.class);
		repo.setRestTemplate(restTemplateMock);
		
		
		when(restTemplateMock.getForObject("http://localhost:8889/proc/env", Configuration.class)).thenThrow(new RuntimeException());
		when(restTemplateMock.getForObject("http://localhost:8888/proc/env", Configuration.class)).thenReturn(null);
		
		repo.getConfig();
	}
	
}
